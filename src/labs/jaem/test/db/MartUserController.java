package labs.jaem.test.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

@Component
public class MartUserController {
	private MartDao dao;
	
	@Autowired 
	
	public MartUserController(MartDao dao)
	{
		this.dao =dao;
	}
	
	public String  getAllUsers()
	{
		System.out.print("this is dao!" + dao + "   " + this);

		String sql = "select id,name,email,pass from tbl_usuarios_spring";
		
		List users = this.dao.executeQuery (sql,null,new UserMapper());
		
		String  return_users = " <table class='table'>";
		
		return_users = return_users + " <tr>" +
   " <th>ID</th>"+
   " <th>Nombre</th> "+
   " <th>Email</th> " +
   " <th>Pass</th> " +
   " </tr>";
		
		for (int i  = 0 ;i< users.size();i++ )
		{
			System.out.println(  ((MartUser)users.get(i)).getEmail() );
			
			return_users = return_users + "<tr>     <td>"+((MartUser)users.get(i)).getId()+"</td>      <td>"+((MartUser)users.get(i)).getName()+"</td>      <td>"+((MartUser)users.get(i)).getEmail()+"</td>  <td>"+((MartUser)users.get(i)).getPass()+"</td>  </tr>";
			
		}
		
		
		return_users = return_users + "</table>";
	
		return return_users;
	}
	
	
	
	public String  getProductsBuy(String products_array,String saldo)
	{
		System.out.print("this is dao!" + dao + "   " + this);

		String sql = "select * from tbl_productos_spring where id_producto in("+products_array+")";
		
		List products = this.dao.executeQuery (sql,null,new ProductMapper());
		
		int total = 0;
		
		String  return_products = " <table class='table'>";
		
		return_products = return_products + " <tr>" +
   " <th>ID</th>"+
   " <th>Producto</th> "+
   " <th>Precio</th> " +
   " </tr>";
		
		for (int i  = 0 ;i< products.size();i++ )
		{

			
			return_products = return_products + "<tr>     <td>"+((MartProduct)products.get(i)).getId()+"</td>      <td>"+((MartProduct)products.get(i)).getName()+"</td>      <td>$"+((MartProduct)products.get(i)).getPrice()+"</td>   </tr>";
			total = total + Integer.parseInt(((MartProduct)products.get(i)).getPrice());
		}
		
		
		return_products = return_products + "</table>";
		

		
		return_products = return_products + "\n" + "<div class='row'><div class='col-md-12' style='text-align:center;'><h1>Su saldo actual es:$</h1><h1 id='saldo'>"+saldo+"</h1></div></div>" ;
		
		return_products = return_products + "\n" + "<div class='row'><div class='col-md-12' style='text-align:center;'><h1>El total de su compra es:$</h1><h1 id='total'>"+total+"</h1></div></div>" ;
	
		return return_products;
	}
	

	
	
	
	
	public String  getProducts(String saldo)
	{
		System.out.print("this is dao!" + dao + "   " + this);

		String sql = "select * from tbl_productos_spring";
		
		List products = this.dao.executeQuery (sql,null,new ProductMapper());
		
		String  return_products = "";
		
		
		for (int i  = 0 ;i< products.size();i++ )
		{
			return_products = return_products + "\n";
			
			//return_products = return_products + "<tr>     <td>"+((MartProduct)products.get(i)).getId()+"</td>      <td>"+((MartProduct)products.get(i)).getName()+"</td>      <td>"+((MartProduct)products.get(i)).getPrice()+"</td>  <td>"+((MartProduct)products.get(i)).getCategory()+"</td>  </tr>";
			
			return_products = return_products + "<div class='element-item col-md-4  "+((MartProduct)products.get(i)).getCategory()+"' data-category='"+((MartProduct)products.get(i)).getCategory()+"'> "+
												"	<a href='javascript:void(0)'>" +
												"	<div class='w3-card-4 test' style='width:100%;max-width:300px;' id='"+((MartProduct)products.get(i)).getId()+"' onclick='AgregarProducto(this)' > " +
												"	  <img src='images/"+((MartProduct)products.get(i)).getPhoto()+"' alt='Avatar' style='width:100%;opacity:0.85;height:200px;'> " +
												"	  <div class='w3-container'> " +
												"	  <h4><b>"+((MartProduct)products.get(i)).getName()+"</b></h4>     " +
												"	  <p>$"+((MartProduct)products.get(i)).getPrice()+"</p>     " +
												"	  </div> " +
												"	</div>" + 
												"	</a> " +
												"	</div>";
		}
		

	
		return return_products;
	}
	
	
	
	public String  getUser(String usuario, String pwd)
	{
		
		String validacion = "";

		String sql = "select id,name,email,pass,saldo from tbl_usuarios_spring where email = '"+usuario+"' and  pass = '"+pwd+"'";
		
		System.out.print(sql);
		
		List users = this.dao.executeQuery (sql,null,new UserMapper());
		
		for (int i  = 0 ;i< users.size();i++ )
		{

			validacion =((MartUser)users.get(i)).getSaldo();
		}
		System.out.print("El saldo es:"+validacion);
		return validacion;
	}
	
	
	public class UserMapper implements RowMapper<MartUser>
	{

		@Override
		public MartUser mapRow(ResultSet data, int num) throws SQLException {
			// TODO Auto-generated method stub
			
			MartUser user= new MartUser();
			user.setId(data.getInt(1));
			user.setName(data.getString(2));
			user.setEmail(data.getString(3));
			user.setPass(data.getString(4));
			user.setSaldo(data.getString(5));
			
			return user;
		}
		
	}
	
	
	
	public class ProductMapper implements RowMapper<MartProduct>
	{

		@Override
		public MartProduct mapRow(ResultSet data, int num) throws SQLException {
			
			MartProduct product= new MartProduct();
			product.setId(data.getInt(1));
			product.setName(data.getString(2));
			product.setPrice(data.getString(3));
			product.setCategory(data.getString(4));
			product.setPhoto(data.getString(5));
			
			return product;
		}
		
	}



	public int setNewBuy(String correo, String productos,String total) {
		 Date ahora = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");

		
		String sql = "insert into tbl_compras_spring VALUES (:usuario, :productos, :monto_compra, :fecha)";
		SqlParameterSource namedParameters = new MapSqlParameterSource();
		((MapSqlParameterSource)namedParameters).addValue("usuario", correo);
		((MapSqlParameterSource)namedParameters).addValue("productos", productos);
		((MapSqlParameterSource)namedParameters).addValue("monto_compra", total);
		((MapSqlParameterSource)namedParameters).addValue("fecha", formateador.format(ahora));
		
		return this.dao.insertData(sql, namedParameters);
		
	}
	
	

	public int updateCash(String attribute, int restante) {
		String sql = "update tbl_usuarios_spring set saldo = :restante where email = :email";
		SqlParameterSource namedParameters = new MapSqlParameterSource();
		((MapSqlParameterSource)namedParameters).addValue("restante", restante);
		((MapSqlParameterSource)namedParameters).addValue("email", attribute);

		
		return this.dao.insertData(sql, namedParameters);
		
	}
	
	
	
	/****************************Angular **************************/
	
	public int setLog(String correo,String cliente,String tipo_request) {
		 Date ahora = new Date();
		 SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");


		String sql = "insert into tbl_log_spring VALUES (:usuario, :fecha,:cliente,:tipo_request)";
		SqlParameterSource namedParameters = new MapSqlParameterSource();
		((MapSqlParameterSource)namedParameters).addValue("usuario", correo);
		((MapSqlParameterSource)namedParameters).addValue("fecha", formateador.format(ahora));
		((MapSqlParameterSource)namedParameters).addValue("cliente", cliente);
		((MapSqlParameterSource)namedParameters).addValue("tipo_request", tipo_request);
		
		return this.dao.insertData(sql, namedParameters);
		
	}

}
