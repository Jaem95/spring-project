package labs.jaem.test;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;

import labs.jaem.test.db.MartUserController;

@Controller

public class MainController {
	
	String correoSesion = "";
	String productosSesion = "";
	int saldoSesion = 0;

	MartUserController userController;
	
	public void  setUserController(MartUserController userController)
	{
		this.userController = userController;
	}
	
	@RequestMapping (value={"/users"})
	
	public ModelAndView getAllUsers(){
		System.out.println("Controller here!" + userController);
		
		String  users = userController.getAllUsers();
		
		String message = "<br><div style='text-align:center;'>"+
						"<h3> ******** Control de Usuarios ******* </div> <br> <br>"+
				         users;
		
		
		return new ModelAndView("mainView", "message",message);
	}
	
	
@RequestMapping (value={"/login"})
	
	public ModelAndView getLogin(){
		System.out.println("Controller here!" + userController);
		
		String message = "";
		
		return new ModelAndView("login", "message",message);
	}
	
	
	@RequestMapping (value={"/user/{name}/{email}/{pass}"})
	public ModelAndView insertUser( @PathVariable(value="name") String n,
	@PathVariable(value="email") String e,
	@PathVariable(value="pass") String p
	)
	{
		System.out.println(n+ e +p);
		String message = "<br><div style='text-align:center;'>"
				+ "<h3>********** Users controller Insertion " + n +"**********</div><br><br>";
				return new ModelAndView("welcome", "message", message);
	}
	
	
	@RequestMapping (value={"/user/{email}/{pass}"})
	public ModelAndView validateLogin(
			@PathVariable(value="email") String e,
			@PathVariable(value="pass") String p,HttpSession session
			)
	{
		
		String  saldo = userController.getUser(e,p);
		int validacion = 0;
		
		
		System.out.print("El saldo es:"+saldo);
		
		if(saldo != null && !saldo.isEmpty())
		{
			validacion =1;
			session.setAttribute("email", e);
			session.setAttribute("pass", p);
			session.setAttribute("saldo", saldo);
		}
		System.out.print("El val es:"+validacion);
			
		return new ModelAndView("loginResponse", "message",validacion);
	}
	
	
	@RequestMapping (value={"/tienda"})
	
	public ModelAndView getProducts(HttpSession session){
		String sesion = (String) session.getAttribute("email");
		if(sesion == null)
		{
			return new ModelAndView("login");
		}
		else
		{
		System.out.println("Controller here!" + userController);
		
		String  products = userController.getProducts((String)session.getAttribute("saldo"));
		
		//products = products + "\n" + "<div class='row'><div class='col-md-12' style='text-align:center;'><h1>Su saldo actual es:$"+(String)session.getAttribute("saldo")+"</h1></div></div>" ;
		
		return new ModelAndView("tienda", "message",products);
		}
		
		
		
	}
	
	@RequestMapping (value={"/compra"})
	
	public ModelAndView getProductsBuy(HttpSession session){
		String sesion = (String) session.getAttribute("email");
		if(sesion == null)
		{
			return new ModelAndView("login");
		}
		else
		{
		System.out.println("Controller here!" + userController);
		
		String products_array = (String)session.getAttribute("products");
		
		String  products = userController.getProductsBuy(products_array,(String)session.getAttribute("saldo"));
				
		
		return new ModelAndView("compra", "message",products);
	}
	}
	
	
	@RequestMapping(value={"/compraPost"})
    @ResponseBody
	public ModelAndView addUser( 
	@RequestParam("datos") String articulos,HttpSession session
	){;
		session.setAttribute("products", articulos);
		return new ModelAndView("compraResponse", "message" , articulos);
	}

	@RequestMapping(value={"/compraFinal"})
    @ResponseBody
	public ModelAndView addBuy( 
	@RequestParam("total") String total,@RequestParam("saldo") String saldo,HttpSession session
	){
		
		int validacion =userController.setNewBuy((String)session.getAttribute("email"),(String)session.getAttribute("products"),total);
		
		int restante = Integer.parseInt(saldo)- Integer.parseInt(total);
		String res = Integer.toString(restante);
		session.setAttribute("saldo", res);
		if (validacion == 1)
		{
			validacion = userController.updateCash((String)session.getAttribute("email"),restante);
		}
		
		return new ModelAndView("compraResponse", "message" , validacion);
	}

	
	@RequestMapping(value={"/logout"})
    @ResponseBody
	public ModelAndView logout(HttpSession session
	){
		session.invalidate();
		return new ModelAndView("login");
	}
	
	
	/*******************************Angular*********************/

	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/userAngular/{email}/{pass}", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String foo
	(
			@PathVariable(value="email") String e,
			@PathVariable(value="pass") String p,HttpServletResponse response,HttpServletRequest request,HttpSession session
	) 
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi correo!" + e + "Recibi password" + p);
		String validacion2 = "";
		int  validacion = userController.setLog(e,userAgent,"Login");
		
		session.setAttribute("userEmail", e);
		
		correoSesion = e;
		
		try {
		
			
			 validacion2 = validarLoginNode(e,p,userAgent);
			
			 
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		saldoSesion = Integer.parseInt(validacion2);
	    return  validacion2 ;
	}
	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/newUser/{name}/{email}/{pwd}", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String addUserAngular
	( 
			@PathVariable("name") String nombre,
			@PathVariable("email") String correo,
			@PathVariable("pwd") String password,
	HttpServletResponse response,HttpServletRequest request
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi correo!" + correo + "Recibi password" + password);
		String validacion2 = "";
		correoSesion = correo;
		try {
			validacion2 = newUser(nombre,correo,password,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int  validacion = userController.setLog(correo,userAgent,"Registro");
	    return  ""+validacion ;
	}
		
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/validateUser/{email}", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String validateUSer
	( 
			@PathVariable("email") String correo,
	HttpServletResponse response,HttpServletRequest request
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi correo!" + correo );
		String validacion2 = "";
	    return  ""+correo ;
	}
		
	
	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/bringProduct/", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String bringProducts
	( 
			HttpServletResponse response,HttpServletRequest request,HttpSession session
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi correo2!" + correoSesion );
		int  validacion = userController.setLog(correoSesion,userAgent,"Revision de Productos");
		String productos = "";
		try {
			 productos = traerProductos(correoSesion,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Codigo !" + productos );
		
		
	    return  productos ;
	}
	
	
	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/bringLog/", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String bringLog
	( 
			HttpServletResponse response,HttpServletRequest request,HttpSession session
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi log!" + correoSesion );
		int  validacion = userController.setLog(correoSesion,userAgent,"Revision de Log");
		String log = "";
		
		try {
			log = traerLog(correoSesion,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Recibi log!" + log );
		
		return log;
	}
	
	
	
	private String traerLog(String correoSesion2,String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/selectPurchaseByUser/"+correoSesion+"/";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/bringPurchase/", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String bringPurchase
	( 
			HttpServletResponse response,HttpServletRequest request,HttpSession session
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi confirmacion de productos!" + correoSesion );
		int  validacion = userController.setLog(correoSesion,userAgent,"Confirmacion de Productos");
		String productos = "";
		/*try {
			 productos = traerCompra(correoSesion,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Codigo !" + productos );
		*/
		
		String products_array = productosSesion;
		
		String  products = userController.getProductsBuy(products_array,""+saldoSesion);
		
	    return  products ;
	}
	
	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/confirmPurchase/{saldo}/{total}", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String confirmPurchase
	( 
			@PathVariable("saldo") String saldo,
			@PathVariable("total") String total,HttpServletResponse response,HttpServletRequest request,HttpSession session
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi confirmacion de compra!" + correoSesion );
		int  validacion = userController.setLog(correoSesion,userAgent,"Confirmacion de Compra");
		
		validacion =userController.setNewBuy(correoSesion,productosSesion,total);

		int restante = Integer.parseInt(saldo)- Integer.parseInt(total);
		String res = Integer.toString(restante);
		saldoSesion = restante;
		if (validacion == 1)
		{
			validacion = userController.updateCash(correoSesion,restante);
		}
		
		String validacion2 = "";
		try {
			validacion2 = actualizarSaldo(correoSesion,restante,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String validacion3  ="";
		
		try {
			validacion3 =  guardarCompraNode(correoSesion,userAgent,total);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	    return  validacion3 ;
	}
	
	

	private String guardarCompraNode(String correoSesion2, String userAgent, String total) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/insertPurchase/"+correoSesion+"/"+total;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	private String actualizarSaldo(String correoSesion,int restante,String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/updateMoney/"+correoSesion+"/"+restante;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
		
	}

	private String traerCompra(String correoSesion2, String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/productsPurchase/"+correoSesion+"";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	
	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/bringInfo/", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String bringInfo
	( 
			HttpServletResponse response,HttpServletRequest request,HttpSession session
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi solicitud de perfil!" + correoSesion );
		int  validacion = userController.setLog(correoSesion,userAgent,"Revision de Perfil");
		String productos = "";
		try {
			 productos = traerInformacion(correoSesion,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Informacion del usuario: !" + productos );
		
		productos = productos +"="+correoSesion;
	    return  productos ;
	}

	
	@CrossOrigin(origins = "http://localhost:2167")
	@RequestMapping(value="/editUser/{name}/{email}/{pwd}", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String editUser
	( 
			@PathVariable("name") String nombre,
			@PathVariable("email") String correo,
			@PathVariable("pwd") String password,
	HttpServletResponse response,HttpServletRequest request
	)
	{
		String userAgent = request.getHeader("User-Agent");
		System.out.println("Recibi correo de correcion" + nombre);
		String validacion2 = "";
		correoSesion = correo;
		try {
			validacion2 = editUser(nombre,correo,password,userAgent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int  validacion = userController.setLog(correo,userAgent,"Modificacion de Perfil");
	    return  ""+validacion2 ;
	}
	
	
	private String editUser(String nombre, String correo, String password, String userAgent) throws Exception {
		nombre = nombre.replace(" ","-");
		String url = "http://10.25.231.129:8081/TasterFaceShop/editUser/"+nombre+"/"+correo+"/"+password+"";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		System.out.println(url);
		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	private String traerInformacion(String correoSesion2, String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/bringUserInfo/"+correoSesion+"";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	
	
	@RequestMapping(value={"/compraPostAngular"})
    @ResponseBody
	public String buyProductsAngular( 
	@RequestParam("datos") String articulos,HttpServletResponse response,HttpServletRequest request,HttpSession session
	){
		String userAgent = request.getHeader("User-Agent");
		int  validacion = userController.setLog(correoSesion,userAgent,"Compra  de Productos");
		session.setAttribute("products", articulos);
		productosSesion = articulos;
		String productos = "";
		try {
			System.out.println("Codigo !" + productos );
			 productos = compraProductos(correoSesion,userAgent,articulos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Codigo2 !" + productos );
		}
		
		
		return articulos;
	}
	
	private String compraProductos(String correoSesion2, String userAgent, String articulos) throws Exception {
		articulos = articulos.replace(',', '=');
		
		String url = "http://10.25.231.129:8081/TasterFaceShop/confirmPurchase/"+articulos;
		
		System.out.println("Recibi productos"+articulos);

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	private String traerProductos(String correoSesion2,String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/selectProducts/"+correoSesion+"";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	private String newUser(String nombre, String correo, String password,String userAgent) throws Exception {
		
		nombre = nombre.replace(" ","-");
		String url = "http://10.25.231.129:8081/TasterFaceShop/newUser/"+nombre+"/"+correo+"/"+password+"";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		System.out.println(url);
		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

	private String validarLoginNode(String e, String p,String userAgent) throws Exception {
		String url = "http://10.25.231.129:8081/TasterFaceShop/login/"+e+"/"+p+"";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", userAgent);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();
	}

}
	





