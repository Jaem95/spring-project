<%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html>
<html lang="en">
<head>
<title>mainView</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
.form-group > .error{
color:red;
}
</style>
</head>
<body>
<div class="container">
${message}

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Registrar Usuario</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Registro de Usuario</h4>
      </div>
      <div class="modal-body">
       <form class="form-horizontal" role="form" id="formRegistro" name="formRegistro" method="post">
       <div class="col-md-12">
       
       	  <div class="form-group">
      <label for="email">Name:</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Enter name"  >
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" >
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter password" >
    </div>
       </div>
  
    
    
    
    <button type="submit" class="btn btn-default" id="btnRegistro">Submit</button>
  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


</div>

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.js"></script>
<script type="text/javascript">
        $("#btnRegistro").click(function () {
            $.validator.addMethod("valueNotEquals", function (value, element, arg) {
                return arg != value;
            }, "Value must not equal arg.");


            $("#formRegistro").validate({
            
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    pwd: {
                        required: true,
                        minlength: 6
                    }  
                },
                // Specify validation error messages
                messages: {
                    name: "Por favor complete su nombre",
                    email: {
                        required: "Por favor complete su correo electronico",
                        email: "Ingrese un direccion valida.",

                    },
                    pwd: {
                        required: "Porfavor complete su contraseņa",
                        minlength: "La contraseņa debe de tener almenos 6 carecteres"
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
					   $.get("/Test/user/"+document.getElementById("name").value+"/"+document.getElementById("email").value+"/"+document.getElementById("pwd").value+"", function(data, status){
						        alert("Data: " + data + "\nStatus: " + status);
					    });

//                              swal({
//                                   title: "Registro Incorrecto",
//                                   text: "El correo que ingreso ya se encuentra registrado, favor de intentar con otro correo",
//                                   type: "warning",
//                                   showCancelButton: false,
//                                   confirmButtonClass: 'btn-info waves-effect waves-light',
//                                   confirmButtonText: 'OK'
//                               });

                }
            });
        });
    </script>

</body>
</html>